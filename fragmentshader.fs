#version 330

precision mediump float;

uniform mat4 modelViewMatrix; //view * model
uniform mat4 modelViewProjectionMatrix; //projection * view * model
uniform mat3 normalMatrix;
uniform vec3 lightNormalPosition;
uniform vec3 lightColouredPosition;
uniform vec3 colouredIntensity;
uniform vec3 calculationVector;
uniform int  shadingType;
uniform vec4 reflectionProperties;
uniform sampler2D myTextureSampler;
uniform sampler2D myNormalMapSampler;
uniform int  hasTexture;
uniform int  hasNormalMap;

in mediump vec3 ourCoords3;
in mediump vec3 inputtnorm;
in mediump vec2 uvCoords;
in vec3 vColor;


out vec4 fragColor;


void main()
{
    vec3 tnorm = inputtnorm;
    if(hasNormalMap == 1) {
      tnorm = (2*texture(myNormalMapSampler, uvCoords)).xyz - vec3(1.0, 1.0, 1.0);
      tnorm = normalize(tnorm);
    }
    if(shadingType == 1) { //phong shading requested
        vec3 l1 = normalize(lightColouredPosition - ourCoords3);
        vec3 l2 = normalize(lightNormalPosition - ourCoords3);
        vec3 v = normalize(-ourCoords3);
        vec3 r1 = reflect(-l1, tnorm);
        vec3 r2 = reflect(-l2, tnorm);
        float sDotN1 = max(dot(l1, tnorm), 0.0);
        float sDotN2 = max(dot(l2, tnorm), 0.0);
        vec3 intensity = vec3(0, 0, 0);
        if(calculationVector.x > 0.5) { //ambient lighting enabled
          intensity += reflectionProperties.x * colouredIntensity;
        }
        if(calculationVector.y > 0.5) { //diffuse lighting enabled
          intensity += reflectionProperties.y * colouredIntensity * sDotN1;
          intensity += reflectionProperties.y * vec3(0.3, 0.3, 0.3) * sDotN2;
        }
        if(calculationVector.z > 0.5) { //specular lighting enabled
          if(sDotN1 > 0.0) {
            intensity += reflectionProperties.z * colouredIntensity * pow(max(dot(r1, v), 0.0), reflectionProperties.w);
          }
          if(sDotN2 > 0.0) {
            intensity += reflectionProperties.z * vec3(0.3, 0.3, 0.3) * pow(max(dot(r2, v), 0.0), reflectionProperties.w);
          }

       }
       if(hasTexture == 1) {
          fragColor = texture(myTextureSampler, uvCoords) * vec4(intensity, 1.0);
       } else {
          fragColor = vec4(vColor * intensity, 1.0);
       }
    } else {
       if(hasTexture == 1) {
          fragColor = texture(myTextureSampler, uvCoords) * vec4(vColor, 1.0);
       } else {
          fragColor = vec4(vColor, 1.0);
       }
    }
}
