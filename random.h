#ifndef MYRANDOM_H
#define MYRANDOM_H

#ifdef __cplusplus
extern "C" {
#endif

void initRandom();
int getRandomNumber(int from, int to);

#ifdef __cplusplus
}
#endif

#endif //MYRANDOM_H
