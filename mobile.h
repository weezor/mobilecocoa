//
//  mobile.h
//  mobilusCocoa
//
//  Created by Felix S on 10/05/14.
//  Copyright (c) 2014 Felix Siefert. All rights reserved.
//

#ifndef mobilusCocoa_mobile_h
#define mobilusCocoa_mobile_h

extern void (*onIdlePointer)(int);
extern void (*onDisplayPointer)(void);
extern void (*onMousePointer)(int, int, int, int);
extern void (*onMouseMotionPointer)(int, int);
extern void (*onKeyboardPointer)(unsigned char, int, int);
extern void (*onKeyboardUpPointer)(unsigned char, int, int);
extern void (*initOwnPointer)(void);;


//static void onIdle();
//static void onDisplay();
//static void onMouse(int button, int state, int x, int y);
//static void onKeyboard(unsigned char key, int x, int y);
//static void initOwn();


#endif
