/******************************************************************
*
* LoadTexture.h
*
* Description: Helper routine for loading texture in BMP format.
* Adapted from texture loading code of the online OpenGL Tutorial:
*
* http://www.opengl-tutorial.org/
*
* Computer Graphics Proseminar SS 2014
* 
* Interactive Graphics and Simulation Group
* Institute of Computer Science
* University of Innsbruck
*
*******************************************************************/

#ifndef __LOAD_TEXTURE_H__
#define __LOAD_TEXTURE_H__

#ifdef __cplusplus
extern "C" {
#endif


/* Structure containing texture RGB array and dimensions  */  
struct TextureData 
{
    unsigned char *data;
    unsigned int width, height;
};

typedef struct TextureData* TextureDataPtr;

/* Load BMP file specified by filename */
int loadTexture(const char* filename, TextureDataPtr data);

#ifdef __cplusplus
}
#endif

#endif // __LOAD_SHADER_H__
