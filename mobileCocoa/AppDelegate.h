//
//  AppDelegate.h
//  mobilusCocoa
//
//  Created by Felix S on 10/05/14.
//  Copyright (c) 2014 Felix Siefert. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
