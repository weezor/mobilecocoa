//
//  resourceHelper.m
//  mobilusCocoa
//
//  Created by Felix S on 11/05/14.
//  Copyright (c) 2014 Felix Siefert. All rights reserved.
//

#import "resourceHelper.h"
#import <Foundation/Foundation.h>

////////////////////////////////////////////////////////////
std::string resourcePath(void)
{
    std::string rpath;
    @autoreleasepool {
        NSBundle* bundle = [NSBundle mainBundle];
        
        if (bundle == nil) {
#ifdef DEBUG
            NSLog(@"bundle is nil... thus no resources path can be found.");
#endif
        } else {
            NSString* path = [bundle resourcePath];
            rpath = [path UTF8String] + std::string("/");
        }

    }
    
    return rpath;
}