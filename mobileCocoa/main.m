//
//  main.m
//  mobilusCocoa
//
//  Created by Felix S on 10/05/14.
//  Copyright (c) 2014 Felix Siefert. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
