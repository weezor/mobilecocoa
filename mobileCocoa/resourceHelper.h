//
//  resourceHelper.h
//  mobilusCocoa
//
//  Created by Felix S on 11/05/14.
//  Copyright (c) 2014 Felix Siefert. All rights reserved.
//

#ifndef RESOURCE_PATH_HPP
#define RESOURCE_PATH_HPP

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <string>

////////////////////////////////////////////////////////////
/// \brief Return the path to the resource folder.
///
/// \return The path to the resource folder associate
/// with the main bundle or an empty string is there is no bundle.
///
////////////////////////////////////////////////////////////
std::string resourcePath(void);

#endif