//
//  MacOpenGLView.m
//  mobilusCocoa
//
//  Created by Felix S on 10/05/14.
//  Copyright (c) 2014 Felix Siefert. All rights reserved.
//

#import "MacOpenGLView.h"
#import <QuartzCore/CVDisplayLink.h>

#include "mobile.h"

@interface MacOpenGLView ()

- (void) initGL;
- (void) drawGL:(double)time;
- (void) windowWillClose:(NSNotification*)notification;

@end

static CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,
									  const CVTimeStamp* now,
									  const CVTimeStamp* outputTime,
									  CVOptionFlags flagsIn,
									  CVOptionFlags* flagsOut,
									  void* displayLinkContext)
{
    @autoreleasepool {
        [(__bridge MacOpenGLView*)displayLinkContext drawGL:(double)outputTime->videoTime / outputTime->videoTimeScale];
    }
    return kCVReturnSuccess;
}


@implementation MacOpenGLView

CVDisplayLinkRef mDisplayLink;

- (void) awakeFromNib
{
    NSOpenGLPixelFormatAttribute attrs[] =
	{
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFADepthSize, 24,
		NSOpenGLPFAOpenGLProfile,
		NSOpenGLProfileVersion3_2Core,
		0
	};
	
	NSOpenGLPixelFormat *pf = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
	
	if (!pf)
	{
		NSLog(@"No OpenGL pixel format");
	}
    
    NSOpenGLContext* context = [[NSOpenGLContext alloc] initWithFormat:pf shareContext:nil];
    
#if defined(DEBUG)
	CGLEnable([context CGLContextObj], kCGLCECrashOnRemovedFunctions);
#endif
	
    [self setPixelFormat:pf];
    
    [self setOpenGLContext:context];
}

- (void) prepareOpenGL //NSOpenGLView override
{
	[super prepareOpenGL];
	
	// Make all the OpenGL calls to setup rendering
	//  and build the necessary rendering objects
	[self initGL];
	
	// Create a display link capable of being used with all active displays
	CVDisplayLinkCreateWithActiveCGDisplays(&mDisplayLink);
	
	// Set the renderer output callback function
	CVDisplayLinkSetOutputCallback(mDisplayLink, &MyDisplayLinkCallback, (__bridge void *)(self));
	
	// Set the display link for the current renderer
	CGLContextObj cglContext = [[self openGLContext] CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = [[self pixelFormat] CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(mDisplayLink, cglContext, cglPixelFormat);
	
	// Activate the display link
	CVDisplayLinkStart(mDisplayLink);
	
	// Register to be notified when the window closes so we can stop the displaylink
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(windowWillClose:)
												 name:NSWindowWillCloseNotification
											   object:[self window]];
}

- (void) windowWillClose:(NSNotification*)notification
{
	// Stop the display link when the window is closing because default
	// OpenGL render buffers will be destroyed.  If display link continues to
	// fire without renderbuffers, OpenGL draw calls will set errors.
	
	CVDisplayLinkStop(mDisplayLink);
}

- (void) initGL
{
    initOwnPointer();
}

- (void) drawGL:(double)time
{
    [[self openGLContext] makeCurrentContext];
    
	// We draw on a secondary thread through the display link
	// When resizing the view, -reshape is called automatically on the main
	// thread. Add a mutex around to avoid the threads accessing the context
	// simultaneously when resizing
	CGLLockContext([[self openGLContext] CGLContextObj]);
    
    int msTime = time * 1000;
    
    onIdlePointer(msTime);
	onDisplayPointer();
    
	CGLFlushDrawable([[self openGLContext] CGLContextObj]);
	CGLUnlockContext([[self openGLContext] CGLContextObj]);

}

- (BOOL)acceptsFirstResponder
{
    return YES;
}

- (void)keyDown:(NSEvent*)event {
    //NSLog(@"%@ %@ - %@", self.className, NSStringFromSelector(_cmd), event);
    //[self interpretKeyEvents:[NSArray arrayWithObject:event]];
    NSString *charString = event.characters;
    onKeyboardPointer([charString characterAtIndex:0], 0, 0);
}

- (void)keyUp:(NSEvent*)event {
    //NSLog(@"%@ %@ - %@", self.className, NSStringFromSelector(_cmd), event);
    //[self interpretKeyEvents:[NSArray arrayWithObject:event]];
    NSString *charString = event.characters;
    onKeyboardUpPointer([charString characterAtIndex:0], 0, 0);
}

- (void)mouseDown:(NSEvent *)theEvent
{
    CGPoint pos = theEvent.locationInWindow;
    onMousePointer(0, 0, pos.x, pos.y);
}

- (void)mouseUp:(NSEvent *)theEvent
{
    CGPoint pos = theEvent.locationInWindow;
    onMousePointer(0, 0, pos.x, pos.y);
}

- (void)mouseDragged:(NSEvent *)theEvent
{
    CGPoint pos = theEvent.locationInWindow;
    onMouseMotionPointer(pos.x, pos.y);
}

@end
